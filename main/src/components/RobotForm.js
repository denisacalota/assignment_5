import React, { Component } from 'react'
import RobotStore from '../stores/RobotStore'

class RobotForm extends Component {
  constructor (props){
    super(props);
    this.state ={
      id:"",
			nameInput: "",
			typeInput: "", 
			massInput: ""
    }

  }
  validate = () => { 
    if (this.state.nameInput!=null && this.state.typeInput !=null && this.state.massInput!=null){
      if(this.state.nameInput.length > 3 && this.state.typeInput.length>3 && this.state.massInput >0 )
      return true;
    }
    return false;
  }
  onChangeName = event=>{
    this.setState({nameInput: event.target.value});
  }
  onChangeType = event =>{
    this.setState({typeInput: event.target.value});
  }
  onChangeMass =event=>{
    this.setState({massInput: event.target.value});
  }

  render() {
    return (
      <div>
        <input 
          type="text" 
          id="name" 
          placeholder= "name" 
          onChange={this.onChangeName} 
          value = {this.state.nameInput}>
        </input>

        <input 
          type="number" 
          id="type" 
          placeholder= "type" 
          onChange={this.onChangeType} 
          value = {this.state.typeInput}>
        </input>

        <input 
          type="text" 
          id="mass" 
          placeholder= "mass" 
          onChange={this.onChangeMass} 
          value = {this.state.massInput}>
        </input>
        
        <input type="button" value="add" onClick= {() => this.props.onAdd({
                id : this.state.id,
                name: this.state.nameInput,
                type: this.state.typeInput,
                mass: this.state.massInput 
              }
            )}
        />

      </div>
    )
  }
}

export default RobotForm;